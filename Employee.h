#ifndef EMPLOYEE_H
#define EMPLOYEE_H

#include <string>

class Employee
{
private:
    int empNum;
    std::string empName;
    double hours;

public:
/* constructors */
    Employee();
    ~Employee();
/* mutators */
    void setEmpNum(int);
    void setEmpName(std::string);
    void setHours(int);
    void addHours(int);
/* accessors */
    int getEmpNum();
    std::string getEmpName();
    int getHours();
};

#endif // EMPLOYEE_H
